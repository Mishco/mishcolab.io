+++
title = "[WIP] Docker Container for test"
date = 2022-07-21
tags = ["Azure", "DevOps", "Pipeline", "CICD", "Docker", "container"]
highlight = true
+++

## Overview

Nowadays, with multiple environments and many developers involved in a single project, it is difficult to properly test everything at a high level. Unit or any small tests are easy to run everywhere nowadays, on a local machine, in the cloud, in the pipeline, etc. But what happens when I want to test some complex functionality or dependencies.

This is where using Docker can help, as it guarantees consistent behavior regardless of the environment. Local machine, cloud or pipeline should have the same result for the same code. And it can also be used as an easier starting point for new novices in any project.

## Existing samples

There are many existing usage of this approach. Most of examples of using docker in devops pipeline are for check, build and push docker images into container repository. For that scenario Azure DevOps offer https://docs.microsoft.com/en-us/azure/devops/pipelines/tasks/build/docker?view=azure-devops&tabs=yaml

Small example, how it can looks like:

```yaml
- task: Docker@2
  displayName: Build and Push
  inputs:
    command: buildAndPush
    repository: repository 
    tags: |
      tag1
      latest
```

However, there are also cases where you want to run this docker container during the pipeline itself. In some cases, the docker container can run without any dependencies. 
Another common use case is to use this isolation from the environment to simulate a perfect testing space. In theory, the test (unit tests) should be logically isolated in the system and should not rely on external systems such as the database, over the network, touching the file system, system configuration.
But in some cases there is necessary to test some from these things. Mock and similar libraries can help, but not for everything. A docker container can help in this marginal situation.

## Docker SonarQube and Unit test for

## Sources

* https://smartbear.com/learn/automated-testing/what-is-unit-testing/
