---
title: "🪞 Repository mirroring (from GitLab to GitHub)"
date: 2021-05-10
tags: ["gitlab", "github", "ci", "mirroring", "repository"]
highlight: true
summary: Make GitLab the main place for all my repositories and GitHub read-only and for archiving purposes.
---


Make GitLab the main place for all my repositories and GitHub read-only and for archiving purposes.

## Step by step

1. Pick up a name (for your repository/repositories): e.g.: `procedural-programming-examples`
2. Create a GitLab repository.
3. Create a GitHub repository.
4. Generate a GitHub personal access token with the `public_repo` box checked (so you can pick up more options), and save that token.
5. On the GitLab repository, go to Settings -> Repository -> Mirroring repositories.
6. Fill in the URL and token. Set the mirror direction to `push`. Set the authentication method to `password` and fill in the `token`.

```bash
https://<your_github_username>@github.com/<again_your_github_username>/<your_github_project>.git
# e.g:
https://Mishco@github.com/Mishco/procedural-programming-examples.git
```
