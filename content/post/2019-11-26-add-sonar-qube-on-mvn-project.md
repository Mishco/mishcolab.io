---
title: "Sonar Qube on Maven/Gradle project"
date: 2019-11-26
tags: ["maven", "sonarqube", "gradle", "docker"]
highlight: true
summary: "Run and check maven or gradle project with Sonar Qube."
---


## Add local SonarQube

### Start sonar qube with docker

    $ docker run -d --name sonarqube -p 9000:9000 sonarqube

## Insert sonar plugin to pom.xml (maven)

```xml

	<build>
         <plugins>
			<plugin>
                 <groupId>org.sonarsource.scanner.maven</groupId>
                 <artifactId>sonar-maven-plugin</artifactId>
                 <version>3.6.0.1398</version>
            </plugin>
        </plugins>
    </build>
```

## Check with sonar quality of your code

    $ mvn sonar:sonar

## Insert into gradle project

```gradle
plugins {
  id "org.sonarqube" version "2.8"
}
```

Or

```gradle
buildscript {
  repositories {
    maven {
      url "https://plugins.gradle.org/m2/"
    }
  }
  dependencies {
    classpath "org.sonarsource.scanner.gradle:sonarqube-gradle-plugin:2.8"
  }
}

apply plugin: "org.sonarqube"
```


After that you can check quality in your project:

    $ gradle sonarqube

## Problem with missing code coverage

https://stackoverflow.com/questions/22174501/sonarqube-not-picking-up-unit-test-coverage

Add `sonar.properties` file to project with information.
Where sonar could find jacoco reports, etc.

## Add remote sonarcloud.io to project

Be careful because sonarcloud.io is for an open source project so everyone can read your code.

First, you need to sign up for sonarcloud with your github (or azure) account.
Generate a token for your project in my site security, site security.
Save this generated token.

Add this section to Travis build file `.travis.yml`:

```yaml
addons:
  sonarcloud:
    organization: "NAME_YOUR_SONARCLOUD_ORGANZITION"
    token:
      secure: $SONAR_TOKEN

script:
 - mvn clean org.jacoco:jacoco-maven-plugin:prepare-agent package sonar:sonar -Dsonar.host.url=https://sonarcloud.io
```
