---
title: "🗣️ German vs English"
date: 2019-11-17T21:27:46+01:00
tags: ["languages", "english", "germany", "Duolingo"]
---

## Duolingo

App for studying new languages

[Duolingo](https://www.duolingo.com/)

Accessible on the Google Play Store: [Google Store](https://play.google.com/store/apps/details?id=com.duolingo&hl=en&referrer=utm_source%3Dduolingo.com%26utm_medium%3Dduolingo_web%26utm_content%3Ddownload_button%26utm_campaign%3Dsplash)

## German Skills

### Duolingo eng <--> ger

| DEU                                            | ENG                                         |
|------------------------------------------------|---------------------------------------------|
| Ich verstehe Deutch                            | I understand English.                       |
| Ich heisse Michal                              | My name is Michal.                         |
| Wie alt bist du?                              | How old are you?                           |
| a Car                                         | das Auto                                   |
| **But I could not talk to you**               | **Aber ich konnte nicht mit dir sprechen** |
| I always wanted to be normal.                  | Ich wollte immer normal sein                |
| **Should we wish him luck?**                  | **Sollen wir ihm Glück wünschen?**         |
| The children play in the snow in December.    | Die Kinder spielen im Dezember im Schnee.   |
| **I do not want to talk about the weather**   | **Ich möchte nicht über das Wetter sprechen.** |
| It was my first position.                       | Es war meine erste Stelle                    |
| Why don't you go in my place?                 |                                             |
| the key                                       | die Taste                                   |
| The social relationships are just as important.| Die sozialen Beziehungen sind genauso wichtig. |
| The man also drinks hot milk                  | Der Mann trinkt auch heiße Milch.          |
| The hot soup is tasty                         | Die heiße Suppe ist lecker.                |
| She is still eating.                          | Sie isst immer noch.                       |
| I am always hungry.                           | Ich habe immer Hunger.                     |
| As of now, I will be using two colors.        | Ab jetzt werde ich zwei Farben verwenden.   |
|                                                | Ihr seht den Markt nicht.                   |
| We had kept the book                          | Wir hatten das Buch behalten.              |
| What will he have said?                       | Was wird er gesagt haben?                  |
| We will have bought the books soon.           | Wir werden die Bücher bald gekauft haben.   |
