---
title: "Transform other markup languages into asciidoc using pandoc"
date: 2020-10-06
tags: ["asciidoc", "markdown", "org", "markup", "pandoc"]
highlight: true
summary: "Here we demonstrates how to use Pandoc, in combination with bash or PowerShell, to convert multiple lightweight markup files between formats such as Markdown, Org-mode, and AsciiDoc, while also providing example commands for batch processing and file migration without Emacs."
---


In all use cases I used [pandoc](https://pandoc.org/). Together with bash or powershell CLI is possible to convert multiple files.

Good examples of comparison of lightweight markup languages:  
(https://en.wikipedia.org/wiki/Lightweight_markup_language) and [link](https://hyperpolyglot.org/lightweight-markup)

## Here's the Pandoc command to convert from the command line

If you would like to convert simple markdown file into org-mode file you can use pandoc.

```bash
pandoc -f markdown -t org -o newfile.org original-file.markdown
```

## Pandoc can convert between multiple document formats

To convert a bunch of Markdown files to org-mode:

```bash
  for f in `ls *.md`; do
    pandoc -f markdown -t org -o ${f}.org ${f};
  done
```

## Migrate from org file (without emacs)

### Export fmr .org to asciidoc or markdown (little step back)

```bash
  iconv -t utf-8 .\file-it-other-encoding.org | pandoc -f org -t asciidoc | iconv -f utf-8 > .\file-in-utf8.org 
```

### Export org (orgmode) to html from vscode
  
```bash
  pandoc atelier.org -o atelier.html
```

## Convert multiple files

Using recursive version of `ls` command.

```bash
for f in `ls -R *.org | awk '/:$/&&f{s=$0;f=0}/:$/&&!f{sub(/:$/,"");s=$0;f=1;next}NF&&f{ print s"/"$0 }'`; do pandoc -f org -t asciidoc -o ${f}.adoc ${f}; done
```

## Sources

- https://emacs.stackexchange.com/questions/5465/how-to-migrate-markdown-files-to-emacs-org-mode-format
