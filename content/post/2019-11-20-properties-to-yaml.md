---
title: 🛠️ Properties to yaml
subtitle: Python script for transformation
summary: In newer version of Spring is more recommended use .yaml for properties than .properties. Properties written in .yaml format are easier to read and update.
date: 2019-11-20
tags: ["spring", "python", "properties", "yaml"]
---

## Introduction

In newer version of Spring is more recommended use .yaml for properties than .properties.
Properties written in .yaml format are easier to read and update.

## Technology stack

Which technology we used:

* Python 3.5 +
* pip

## Build

    $ git clone https://gitlabci.exxeta.com/misl/properties_to_yaml.git and cd properties_to_yaml

Activate your virtualenv.

Run and install dependencies:

    $ pip install -r requirements.txt

## Usage

Run script with path of properties file (.properties/.ini)

    $ python properties_to_yaml.py <NAME_OF_FILE>.properties

Created file should appear next to existing file.

### Properties to yaml

application.properties convert into application.yml is doing by simple python script

### .INI to yaml

Older properties format, but still used, is .ini. That have similar structure than .yaml file

## Deep into Code

For transformation all items in properties is created for loop and inside of loop is called `update_dict()`.

```python
# python 3.8+ compatibility
try:
    collectionsAbc = collections.abc
except:
    collectionsAbc = collections

def update_dict(original_dict, updated_dict):
    """
    Based on http://stackoverflow.com/a/3233356
    """
    for k, v in six.iteritems(updated_dict):
        dv = original_dict.get(k, {})
        if not isinstance(dv, collectionsAbc.Mapping):
            original_dict[k] = v
        elif isinstance(v, collectionsAbc.Mapping):
            original_dict[k] = update_dict(dv, v)
        else:
            original_dict[k] = v
```
