+++
title = "Markdown or Asciidoctor with Latex"
date = 2019-12-11
tags = ["asciidoctor", "LaTeX", "markdown"]
math = true
+++

## Introduction

In this post, I would like to present how LaTeX can be used with Asciidoctor and displayed on a statically generated webpage.

For a basic LaTeX or TeX logo:

$$
\LaTeX
\
\TeX
$$

KaTeX can be used to generate complex math formulas. It supports in-line math using the `\\( ... \\)` delimiters, like this: `\\( E = mc^2 \\)`. By default, it does *not* support in-line delimiters `$...$` because those occur too commonly in typical webpages. It supports displayed math using the `$$` or `\\[...\\]` delimiters, like this:

Formula 1:

$$
\phi = \frac{(1+\sqrt{5})}{2} = 1.6180339887\cdots
$$

### Number Theory

The equation \( a^2 + b^2 = c^2 \) has infinitely many non-proportional integer solutions. The integer solutions of the equation

$$
a^3 + b^3 = c^3
$$

are trivial: at least one entry is zero and the others are "obvious."

### Calculus

A definite integral:

$$
\int_0^1 x^n dx = \frac{1}{n}
$$

The fundamental theorem of calculus:

$$
\frac{d}{dx} \int_a^x f(t) dt = f(x)
$$

### Linear Algebra

A matrix:

$$
M = \left[
\begin{array}{ c c } 1 & 2 \ 3 & 4 \end{array} \right]
$$
