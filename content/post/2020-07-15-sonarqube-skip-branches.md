---
title: "Scan SonarQube branches and pull request branches without paid licenses"
date: 2020-07-15
tags: ["maven", "sonarqube", "devops", "azure"]
highlight: true
summary: "Use SonarQube task in pipeline to check branches and pull request branches without licenses"
---


## Definition of problem

When you would like to scan your code with link:https://www.sonarqube.org/[SonarQube] and you have running on your own environment, sometimes you need to scan not master branches. Idea is that only "clean" code should be in master branch.
By default SonarQube Developer editon does not allow you to scan branches, only master branch. So there are two options, one is to buy licenses for Developer or Enterprise version of SonarQube or remove this information from CI process. The result will be that every branch will create "one new project" in SonarQube report.

## Possible solution

So when you would like to remove this information from your pipeline, e.g. Azure DevOps pipeline: you can add task for remove this information:

If you want to remove branch information

```yaml
- powershell: |
    $params = "$env:SONARQUBE_SCANNER_PARAMS" -replace '"sonar.branch.name":"[\w,/,-]*"\,?'
    Write-Host "##vso[task.setvariable variable=SONARQUBE_SCANNER_PARAMS]$params"
  displayName: "Remove branches info" 
```

And similar when you want to remove pull request info:

```yaml
- powershell: |
    $params = "$env:SONARQUBE_SCANNER_PARAMS" -replace '"sonar.pullrequest.*":"[\w,/,-]*"\,?'
    Write-Host "##vso[task.setvariable variable=SONARQUBE_SCANNER_PARAMS]$params"
  displayName: "Remove pull request info" 
```

## Sources

Discussion about that:

* https://docs.sonarqube.org/latest/branches/overview/
* https://docs.sonarqube.org/latest/branches/branches-faq/
* https://community.sonarsource.com/t/error-to-use-the-property-sonar-branch-name-and-analyze-branches/11962/31
