---
title: Nameko with Spring Boot
comments: true
date: 2019-11-18
tags: ["java", "python", "microservices", "docker", "docker-compose"]
---


NOTE: Full code is not available. Only piece of code will be showed, so try to learn and create for yourself.

## Technology stack

### Python

* Python 3.7
* [Nameko framework](https://www.nameko.io/)
* Spring Boot 2.2
* RabbitMQ
* Redis
* Docker

### Nameko framework

Nameko is a framework for building microservices in Python.

It comes with built-in support for:

* RPC over AMQP
* Asynchronous events (pub-sub) over AMQP
* Simple HTTP GET and POST
* Websocket RPC and subscriptions (experimental)

Nameko is designed to help you create, run and test microservices.

### Spring Boot

Spring Boot makes it easy to create stand-alone, production-grade Spring based Applications that you can "just run".

### Docker and docker-compose

Docker provides a way to run applications securely isolated in a container, packaged with all its dependencies and libraries.

Example of Dockerfile:

```Dockerfile
FROM ubuntu:18.04
COPY . /app
RUN make /app
CMD python /app/app.py
```

#### Docker-compose

Compose is a tool for defining and running multi-container Docker applications.
With Compose, you use a YAML file to configure your application’s services.
Then, with a single command, you create and start all the services from your configuration.
To learn more about all the features of Compose, see the list of features.

```yaml
version: "3"
services:
  web:
    build: .
    ports:
      - "80:8000"
    depends_on:
      - "db"
    command: ["./wait-for-it.sh", "db:5432", "--", "python", "app.py"]
  db:
    image: postgres
```

## Microservices

> **Quote by Martin Fowler:**
>
> The microservice architectural style is an approach to developing a single application as a suite of small services, each running in its own process and communicating with lightweight mechanisms, often an HTTP resource API. These services are built around business capabilities and independently deployable by fully automated deployment machinery. There is a bare minimum of centralized management of these services, which may be written in different programming languages and use different data storage technologies.

### Usage

Communication between microservices is made with RabbitMQ:

* (Python + Redis): Airport, Trip, Customer
* (Java): Fibonacci

## Running Solution

```bash
$ docker-compose -f docker-compose.yml -f docker-compose-add-spring-boot.yml up -d --build
```
