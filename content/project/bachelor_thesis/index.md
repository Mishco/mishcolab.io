---
title: Bachelor Thesis
summary: My bachelor thesis about Shared Mobile
date: 2016-05-30
tags: [ mobile, school project, sharing, telecommunication services, android, multi client ]
---

## Abstract

It can be demanding to work with terminal equipment by telecommunication operators’ services.
The broad functionality provided by the current equipment offers a big comfort in communication, work and entertainment
as well.
Availability of alternative communication interfaces and communication networks allow us to use these devices also in a
so-called "shared mode".
This work analyzes all the existing applications that allow sharing telecommunications services.
The analysis provides a broad overview of the current state and presents new sharing options.
It is then followed by a design and implementation of custom application that allows sharing the telecommunications
services,
such as: phone calls, sending and receiving SMS.
The emphasis is put on sharing services mobile device with the Android operating system which allows sharing among
multiple clients.
In conclusion the thesis offers additional possibilities for the development of such applications.

## Technology stack

* Java 8
* Android

## Whole document (in slovak language)

[Link to document](./bakalarska_praca_1.4.pdf)
