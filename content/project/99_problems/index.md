---
title: 99 Problems
summary: My repository for 99 (most common) problems. Ideally for learning new langauge
date: 2020-10-24
tags: [ java, learning project, asciidoc, junit5, maven, java13, sonarqube, travis-ci ]
---

## About 99-problems

This is an adaptation of the [Ninety-Nine Prolog Problems](https://sites.google.com/site/prologsite/prolog-problems)
written by Werner Hett at the Berne University of Applied Sciences in Berne, Switzerland.

The main purpose for this repository is to train Java skills and also deepen TDD
skills [Test Driven Development](https://martinfowler.com/bliki/TestDrivenDevelopment.html).

## Code base

[99-problems](https://gitlab.com/Mishco/99-problems)

## SonarQube code quality

I like SonarQube for getting fast response to your code quality (bugs, typos, code coverage, ...)

Definition in pipeline is simple for gitlab `.gitlab-ci.yml`

```yaml
sonarcloud-check:
  image: maven:latest
  stage: test
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script:
    - mvn clean org.jacoco:jacoco-maven-plugin:prepare-agent install verify sonar:sonar -Dsonar.host.url=$SONAR_HOST_URL -Dsonar.login=$SONAR_TOKEN
  only:
    - master
    - develop
```

Or travis (`.travis.yaml`):

```yaml
script:
  - mvn clean org.jacoco:jacoco-maven-plugin:prepare-agent install sonar:sonar -Dsonar.projectKey=$PROJECT_KEY
```



