---
title: Diploma thesis
subtitle: "Identification of Users During Usage of Mobile Devices Based on Behavioral Biometrics"
date: 2018-06-30
tags: [ Biometrics, Mobile app, school project, Behavioral biometrics, Mobile devices, Identification, Authentication ]
summary: "My diploma thesis about Identification of Users During Usage of Mobile Devices Based on Behavioral Biometrics"
---

## Abstract

On mobile devices users perform various activities associated with using authentification methods.
Mobile devices also offer authentification through biometric characteristics such as fingerprint or face recognition.
These characteristics require hardware which can obtain and process them.
In case of behavioral characteristics (for example movement of hand with mobile or touch screen detection), any mobile
device has a simpler way of acquiring and processing biometric characteristics.
Due to hardware differences between mobile devices, the user's biometric characteristics may vary considerably, which
may lead to the increase of error rates in the process of user authentification.
This work deals with the analysis of solutions that could acquire and process biometric characteristics from various
mobile devices and correct identify users.
The aim of the thesis is to find a user model independent of the device by means of which we can identify the user on
different devices.
The proposed experiment was acquired the characteristics that enable us to create the right user model.
We can use the user model to identify the user on different devices.

## Technology stack

* Python 3.* (jupyter and later with flask server)
* Android 4.* and higher (for mobile app)
* js, php (for server side and processing data)

## Diagram (in Slovak language)

![img.png](img.png)

## Code

## Whole document

Document is private due to Slovak university of technology, but I can share
introduction for [Motivation and Introduction](./VYSKUM.pdf)

## Presentation (in Slovak language)

[Link to presentation](./xslovikm_DpFinal.pdf)
