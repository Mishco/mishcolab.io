---
title: Kukacky
summary: "Simple desktop app simulated hour bell"
date: 2018-08-30
tags: [ desktop app, java, javafx ]
---

![Clocks](http://pluspng.com/img-png/clock-png-clock-png-image-1478.png)

## Kukacky

Desktop application to simulate bell.
[Code repository on develop](https://gitlab.com/Mishco/kukacky/tree/develop)

## Technology stack

* Java 8
* Sound library
* Javafx for GUI

## How its works

Every hour play short sound of bell (on 3pm repeat the sound three times) and every fifteen minutes play shorter sound.

## Code examples

Using javafx in .fxml file:

```java
    ... 
    <AnchorPane minHeight="0.0"minWidth="0.0"prefHeight="382.0"prefWidth="224.0">
          <children>
            <ChoiceBox id="choiceBox"fx:id="choiceBoxMain"layoutX="14.0"layoutY="28.0"prefWidth="145.0">
              <items>
                <FXCollections fx:factory="observableArrayList">
                  <String fx:value="Item 1"/>
                  <String fx:value="Item 2"/>
                  <String fx:value="Item 3"/>
                </FXCollections>
              </items>
            </ChoiceBox>
        ...
```

