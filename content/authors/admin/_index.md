---
# Display name
title: Michal Slovík

# Name pronunciation (optional)
# name_pronunciation: Chien Shiung Wu

# Full name (for SEO)
first_name: Michal
last_name: Slovik

# Status emoji
status:
icon: ☕️

# Is this the primary user of the site?
superuser: true

# Highlight the author in author lists? (true/false)
highlight_name: true

# Role/position/tagline
role: Developer

# Organizations/Affiliations to display in Biography blox
organizations:
- name: Exxeta s.r.o.
  url: https://exxeta.com/

# Social network links
# Need to use another icon? Simply download the SVG icon to your `assets/media/icons/` folder.
profiles:
- icon: brands/x
  url: https://twitter.com/GetResearchDev
- icon: brands/instagram
  url: https://www.instagram.com/
- icon: brands/github
  url: https://github.com/Mishco/
- icon: brands/linkedin
  url: https://www.linkedin.com/in/michal-slov%C3%ADk-8a105a146/

interests:
- DevOps
- Kubernetes 
- Java, Python
- Terraform
- Clouds (AWS, Azure) 
- Pipelines CI/CD solutions

education:
- area: Master degree in Intelligent Software Systems
  institution: Faculty of Informatics and Information Technologies STU in Bratislava
  date_start: 2016-09-01
  date_end: 2018-07-30
  text: 'Read Thesis'
  url: 'https://example.com'
- area: Bachelor Degree in Informatics
  institution: Faculty of Informatics and Information Technologies STU in Bratislava
  date_start: 2013-09-01
  date_end: 2016-07-30
work:
- position: Consultant
  company_name: Exxeta s.r.o
  company_url: ''
  company_logo: ''
  date_start: 2018-01-01
  date_end: ''

# Skills
# Add your own SVG icons to `assets/media/icons/`
skills:
- name: Technical Skills
  items:
    - name: Python
      description: ''
      percent: 80
      icon: code-bracket
    - name: Java
      description: ''
      percent: 80
      icon: chart-bar
    - name: Docker, kubernetes
      description: ''
      percent: 90
      icon: circle-stack
- name: Hobbies
  color: '#eeac02'
  color_border: '#f0bf23'
  items:
    - name: Hiking
      description: ''
      percent: 60
      icon: person-simple-walk
    - name: Photography
      description: ''
      percent: 80
      icon: camera

languages:
- name: English
  percent: 100
- name: Czech
  percent: 75
- name: German
  percent: 45
  

# Awards.
#   Add/remove as many awards below as you like.
#   Only `title`, `awarder`, and `date` are required.
#   Begin multi-line `summary` with YAML's `|` or `|2-` multi-line prefix and indent 2 spaces below.
awards:
- title: Neural Networks and Deep Learning
  url: https://www.coursera.org/learn/neural-networks-deep-learning
  date: '2023-11-25'
  awarder: Coursera
  icon: coursera
  summary: |
    I studied the foundational concept of neural networks and deep learning. By the end, I was familiar with the significant technological trends driving the rise of deep learning; build, train, and apply fully connected deep neural networks; implement efficient (vectorized) neural networks; identify key parameters in a neural network’s architecture; and apply deep learning to your own applications.
- title: Blockchain Fundamentals
  url: https://www.edx.org/professional-certificate/uc-berkeleyx-blockchain-fundamentals
  date: '2023-07-01'
  awarder: edX
  icon: edx
  summary: |
  Learned:
    - Synthesize your own blockchain solutions
    - Gain an in-depth understanding of the specific mechanics of Bitcoin
    - Understand Bitcoin’s real-life applications and learn how to attack and destroy Bitcoin, Ethereum, smart contracts and Dapps, and alternatives to Bitcoin’s Proof-of-Work consensus algorithm
- title: 'Object-Oriented Programming in R'
  url: https://www.datacamp.com/courses/object-oriented-programming-with-s3-and-r6-in-r
  certificate_url: https://www.datacamp.com
  date: '2023-01-21'
  awarder: datacamp
  icon: datacamp
  summary: |
    Object-oriented programming (OOP) lets you specify relationships between functions and the objects that they can act on, helping you manage complexity in your code. This is an intermediate level course, providing an introduction to OOP, using the S3 and R6 systems. S3 is a great day-to-day R programming tool that simplifies some of the functions that you write. R6 is especially useful for industry-specific analyses, working with web APIs, and building GUIs.
---

## About Me

I am a backend developer at Exxeta s.r.o, with a main focus on technological innovation and testing interesting solutions. And I am passionate about sharing and exchanging knowledge with others.

For more details please visit code repositories at [Github](https://github.com/Mishco) or [GitLab](https://gitlab.com/Mishco/).