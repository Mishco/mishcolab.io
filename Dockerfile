FROM registry.gitlab.com/pages/hugo/hugo_extended:latest

RUN apk add asciidoctor
RUN gem install rouge 
RUN gem install asciidoctor-katex 
RUN gem install asciidoctor-html5s


COPY ./ /app

WORKDIR /app

CMD ["hugo", "serve"]
