+++
# A Skills section created with the Featurette widget.
widget = "featurette"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 30  # Order that this section will appear.

title = "Skills"
subtitle = ""

# Showcase personal skills or business features.
# 
# Add/remove as many `[[feature]]` blocks below as you like.
# 
# For available icons, see: https://sourcethemes.com/academic/docs/widgets/#icons

[[feature]]
  icon = "java"
  icon_pack = "fab"
  name = "Java"
  description = "90%"
  
[[feature]]
  icon = "docker"
  icon_pack = "fab"
  name = "Docker and Kubernetes"
  description = "90%"  
  
[[feature]]
  icon = "python"
  icon_pack = "fab"
  name = "Python"
  description = "70%"  

[[feature]]
  icon = "cloud"
  icon_pack = "fas"
  name = "Cloud solutions, CI/CD, Azure, CloudFoundry,..."
  description = "60%"  

[[feature]]
  icon = "linux"
  icon_pack = "fab"
  name = "Linux (Ubuntu, CentOS, Debian, Arch)"
  description = "50%"

[[feature]]
  icon = "camera-retro"
  icon_pack = "fas"
  name = "Photography"
  description = "20%"

+++
