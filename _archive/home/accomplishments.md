+++
# Accomplishments widget.
widget = "accomplishments"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 50  # Order that this section will appear.

title = "Accomplish&shy;ments"
subtitle = ""

# Date format
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Accomplishments.
#   Add/remove as many `[[item]]` blocks below as you like.
#   `title`, `organization` and `date_start` are the required parameters.
#   Leave other parameters empty if not required.
#   Begin/end multi-line descriptions with 3 quotes `"""`.

[[item]]
  organization = "OCA | Oracle University"
  organization_url = "https://education.oracle.com/products/trackp_333"
  title = "Oracle Certified Associate, Java SE 8 Programmer"
  url = ""
  certificate_url = "https://www.youracclaim.com/badges/efd56e40-b68a-4764-9468-ff1958a677a1/public_url" 
  date_start = "2019-06-01"
  date_end = ""
  description = "Oracle Certified Associate, Java SE 8 Programmer I"

[[item]]
  organization = "Udemy"
  organization_url = "https://www.udemy.com/"
  title = "Docker and Kubernetes: The Complete Guide "
  url = "https://www.udemy.com/course/docker-and-kubernetes-the-complete-guide/"
  certificate_url = "https://www.udemy.com/certificate/UC-V99SJ0O9/"
  date_start = "2018-08-04"
  date_end = ""
  description = "Build, test, and deploy Docker applications with Kubernetes while learning production-style development workflows"

[[item]]
  organization = "Udemy"
  organization_url = "https://www.udemy.com/"
  title = "Spring Framework 5: Beginner to Guru"
  url = "https://www.udemy.com/share/1013MwB0ATd11TQ3o=/"
  certificate_url = ""
  date_start = "2019-06-10"
  date_end = ""
  description = "Spring Framework 5: Learn Spring Framework 5, Spring Boot 2, Spring MVC, Spring Data JPA, Spring Data MongoDB, Hibernate"
  
[[item]]
  organization = "EdX"
  organization_url = "https://courses.edx.org"
  title = "Introduction to Kubernetes"
  url = "https://courses.edx.org/courses/course-v1:LinuxFoundationX+LFS158x+2T2019/course/"
  certificate_url = "https://www.datacamp.com"
  date_start = "2019-09-26"
  date_end = ""
  description = "The Certified Kubernetes Administrator (CKA) program was created by the Cloud Native Computing Foundation (CNCF), in collaboration with The Linux Foundation, to help develop the Kubernetes ecosystem. As one of the highest velocity open source projects, Kubernetes use is exploding."

[[item]]
  organization = "AZ-303"
  organization_url = "https://docs.microsoft.com/en-us/learn/certifications/exams/az-303"
  title = "Microsoft Azure Architect Technologies"
  url = "https://docs.microsoft.com/en-us/learn/certifications/exams/az-303"
  certificate_url = "https://docs.microsoft.com/en-us/learn/certifications/exams/az-303"
  date_start = "2021-04-23"
  date_end = ""
  description = "Candidates for this exam should have subject matter expertise in designing and implementing solutions that run on Microsoft Azure, including aspects like compute, network, storage, and security. Candidates should have intermediate-level skills for administering Azure. Candidates should understand Azure development and DevOps processes."

[[item]]
  organization = "AZ-104"
  organization_url = "https://learn.microsoft.com/en-us/certifications/exams/az-104/"
  title = "Microsoft Azure Administrator"
  url = "https://learn.microsoft.com/en-us/certifications/exams/az-104/"
  certificate_url = "https://www.credly.com/badges/a5478392-6788-49ba-8c6d-e9e9924c8440/public_url"
  date_start = "2023-03-01"
  date_end = "2024-03-02"
  description = "Candidates for the Azure Administrator Associate certification should have subject matter expertise in implementing, managing, and monitoring an organization’s Microsoft Azure environment, including virtual networks, storage, compute, identity, security, and governance. An Azure administrator often serves as part of a larger team dedicated to implementing an organization's cloud infrastructure. Azure administrators also coordinate with other roles to deliver Azure networking, security, database, application development, and DevOps solutions. Candidates for this certification should be familiar with operating systems, networking, servers, and virtualization. In addition, professionals in this role should have experience using PowerShell, Azure CLI, the Azure portal, Azure Resource Manager templates (ARM templates), and Microsoft Azure Active Directory (Azure AD), part of Microsoft Entra."


+++

