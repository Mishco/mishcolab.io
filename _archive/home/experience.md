+++
# Experience widget.
widget = "experience"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 40  # Order that this section will appear.

title = "Experience"
subtitle = ""

# Date format for experience
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Experiences.
#   Add/remove as many `[[experience]]` blocks below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin/end multi-line descriptions with 3 quotes `"""`.
[[experience]]
  title = "Java Developer, Consultant"
  company = "EXXETA s.r.o."
  company_url = "https://www.exxeta.com/en/company/exxeta-sro/"
  location = "Bratislava"
  date_start = "2018-03-01"
  date_end = ""
  description = """
  Responsibilities include:
  
  * Java development
  * Cloud configuration, CI/CD pipelines
  * Docker and Kubernetes solutions
  """

[[experience]]
  title = "Tech support"
  company = "SAS Slovakia s.r.o"
  company_url = "https://www.sas.com/sk_sk/home.html"
  location = "Bratislava"
  date_start = "2016-05-01"
  date_end = "2017-12-31"
  description = """Technical support intern."""

+++
