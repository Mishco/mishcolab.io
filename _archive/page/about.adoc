---
title: About me
subtitle: Few basic information about me
comments: false
---

Welcome to my personal blog! I am Michal Slovík – a really big fan of IT. My website offers an overview of all my projects (school projects, personal, work), technology news and another interesting information I would like to share with you.

For more details about all my project you can check my repositories on github or gitlab.

