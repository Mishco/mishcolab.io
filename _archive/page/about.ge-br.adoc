---
title: About me
subtitle: Einige grundlegende Informationen über mich
comments: false
---

Willkommen in meinem persönlichen Blog! Ich bin Michal Slovík - ein wirklich großer Fan von IT. Meine Website bietet einen Überblick über alle meine Projekte (Schulprojekte, Personal, Arbeit) Technologie-News und weitere interessante Informationen, die ich Ihnen mitteilen möchte.

Für weitere Informationen zu meinem gesamten Projekt können Sie meine Repositorys auf Github oder Gitlab überprüfen.


