+++ 
date = 2019-11-16T19:53:26+01:00
title = "My First Post"
description = "About my first post"
slug = "" 
tags = ["md", "blog"]
categories = []
externalLink = ""
series = []
+++

:source-highlighter: rouge
:rouge-style: molokai
:icons: font

## My first post

This is my first post, how exciting!

### Heading 3
Testing what markdown can do.

#### Heading 4

```java
public static void main(String[] args){
    println("Hello World");
}
```

# Big Heading

**Table**
Here's a useless table:
 
| Number | Next number | Previous number |
| :------ |:--- | :--- |
| Five | Six | Four |
| Ten | Eleven | Nine |
| Seven | Eight | Six |
| Two | Three | One |
 

How about a yummy crepe?

![Crepe](http://s3-media3.fl.yelpcdn.com/bphoto/cQ1Yoa75m2yUFFbY2xwuqw/348s.jpg)


//:stylesheet: css/asciidoctor.css

:source-highlighter: rouge
:rouge-style: molokai
:icons: font

:toc:


## Heading 1

Asciidoctor blog.

### Heading 2

Code example of yaml

[source,yaml]
----
services:
    db:
        image:
        context: Dockerfile
        enviroment:
            - SPRING_PROFILE="test"
----

### Heading Table


Table example:

|===
| Cell in column 1, row 1 | Cell in column 2, row 1
| Cell in column 1, row 2 | Cell in column 2, row 2
| Cell in column 1, row 3 | Cell in column 2, row 3
|===


NOTE: An admonition paragraph draws the reader's attention to
auxiliary information.




